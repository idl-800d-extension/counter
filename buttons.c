#include <stdbool.h>
#include <math.h>

#include "buttons.h"
#include "display.h"

bool inc_button_pressed = true, dec_button_pressed = true, reset_button_pressed = true;

extern int8_t score;

void init_buttons() {
    ADCON1 = 0x0F;
    CMCON = 0x07;

    INC_BUTTON_TRIS = 1;
    DEC_BUTTON_TRIS = 1;
    RESET_BUTTON_TRIS = 1;
}

void process_increment_button(void (*onPressed)()) {
    if (INC_BUTTON && !inc_button_pressed) {
        __delay_ms(1);
        if (INC_BUTTON) {
            (*onPressed)();
        }
    }
    inc_button_pressed = INC_BUTTON;
}

/*
void process_inc_button() {
    if (INC_BUTTON && !inc_button_pressed) {
        __delay_ms(1);
        if (INC_BUTTON) {
            callback();
        }
    }
    inc_button_pressed = INC_BUTTON;
}*/

void process_dec_button() {
    if (DEC_BUTTON && !dec_button_pressed) {
        __delay_ms(1);
        if (DEC_BUTTON) {
            score--;
            
            if(fabs(score) == 10){
                score = -1 * ++score;
            }
            
            int digit = digit_to_symbol(fabs(score));

            set_all_display_symbols(_OFF, _OFF, _OFF, digit);
            if(score < 0) set_display_symbol(_MINUS, 0);
        }
    }
    dec_button_pressed = DEC_BUTTON;
}

void process_reset_button() {
    if (RESET_BUTTON && !reset_button_pressed) {
        __delay_ms(1);
        if (RESET_BUTTON) {
            score = 0;
            set_all_display_symbols(_I, _D, _L, _E);
        }
    }
    reset_button_pressed = RESET_BUTTON;
}

void process_front_buttons() {
    //process_inc_button();
    process_dec_button();
    process_reset_button();
}
