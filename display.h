#ifndef DISPLAY_H
#define	DISPLAY_H

#include <xc.h> 
#include <stdint.h>
#include <stdbool.h>

#include "main.h"

/*
 * RA0 - DS
 * RA1 - STCP
 * RA2 - SHCP
 *
 * RA3 - digit 1
 * RA4 - digit 2
 * RA5 - digit 3
 * RC0 - digit 4
*/
#define DIG1 PORTAbits.RA3
#define DIG1_TRIS TRISA3

#define DIG2 PORTAbits.RA4
#define DIG2_TRIS TRISA4

#define DIG3 PORTAbits.RA5
#define DIG3_TRIS TRISA5

#define DIG4 PORTCbits.RC0
#define DIG4_TRIS TRISC0

#define SH_CP_DIG PORTAbits.RA2
#define SH_CP_DIG_TRIS TRISA2

#define DS_DIG PORTAbits.RA0
#define DS_DIG_TRIS TRISA0

#define ST_CP_DIG PORTAbits.RA1
#define ST_CP_DIG_TRIS TRISA1

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    const uint8_t _OFF = 0; //All segments disabled 
    const uint8_t _ON = 0xFF; //All segments disabled 
    
    //Digits
    const uint8_t _ZERO = 0b11111100;
    const uint8_t _ONE = 0b01100000;
    const uint8_t _TWO = 0b11011010;
    const uint8_t _THREE = 0b11110010;
    const uint8_t _FOUR = 0b01100110; 
    const uint8_t _FIVE = 0b10110110; 
    const uint8_t _SIX = 0b10111110;
    const uint8_t _SEVEN = 0b11100000; 
    const uint8_t _EIGHT = 0b11111110; 
    const uint8_t _NINE = 0b11110110;
    
    //Characters
    const uint8_t _MINUS = 0b00000010;
    
    //Letters
    const uint8_t _A = 0b11101110; 
    const uint8_t _B = 0b00111110; 
    const uint8_t _C = 0b10011100; 
    const uint8_t _D = 0b01111010;
    const uint8_t _E = 0b10011110; 
    const uint8_t _F = 0b10001110; 
    
    const uint8_t _I = 0b01100000;
    const uint8_t _L = 0b00011100;
    
    void init_display();
    
    void set_display_symbol( uint8_t symbol, uint8_t digit );
    
    void set_all_display_symbols( uint8_t symbol1, uint8_t symbol2, uint8_t symbol3, uint8_t symbol4 );
    
    void show_display();
       
    void commit_display();
    
    void shift_display_left( uint8_t n );
    
    uint8_t get_display_symbol( uint8_t digit );
    
    uint8_t digit_to_symbol( uint8_t digit );
    
#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

