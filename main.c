#include <xc.h>
#include <stdint.h>

#include "main.h"

#include <math.h>

#include "display.h"
#include "buttons.h"

//to readme
//or all defines to put in one place
/*
 * RC6 - increment button
 * RC5 - decrement button
 * RC4 - reset button
 * 
 * RA0 - DS
 * RA1 - STCP
 * RA2 - SHCP
 * 
 * RA3 - digit 1
 * RA4 - digit 2
 * RA5 - digit 3
 * RA6 - digit 3 RC0
 */

int8_t score = 0;

void idle() {
    set_display_symbol(_I, 0);
    set_display_symbol(_D, 1);
    set_display_symbol(_L, 2);
    set_display_symbol(_E, 3);
}

void initialize(void) {
    TRISC3 = 0;
    PORTCbits.RC3 = 0;
   
    init_display();
    init_buttons();
    
    idle();
}

void increment_button_pressed() {
            score++;
            
            if(fabs(score) == 10){
                score = -1 * --score;
            }
            
            int digit = digit_to_symbol(fabs(score));

            set_all_display_symbols( score < 0? _MINUS : _OFF, _OFF, _OFF, digit);
}

void main(void) {
    
    initialize();
    
    while(1) {
        show_display();
        process_front_buttons();
        
        process_increment_button(increment_button_pressed);
        
        PORTCbits.RC3 = !PORTCbits.RC3;
        
        //__delay_us(2000);
    }
    
    return;
}



//to put logic in callback-functions here 