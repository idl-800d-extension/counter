#ifndef BUTTONS_H
#define	BUTTONS_H

#include <xc.h>

#define INC_BUTTON_TRIS TRISC6
#define INC_BUTTON PORTCbits.RC6

#define DEC_BUTTON_TRIS TRISC5
#define DEC_BUTTON PORTCbits.RC5

#define RESET_BUTTON_TRIS TRISC4
#define RESET_BUTTON PORTCbits.RC4

void init_buttons();

void process_front_buttons();

void process_increment_button(void (*onPressed)());
void process_decrement_button(void (*onPressed)());

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif

