#include <xc.h>

#include "display.h"

#define SHIFT_IMPULSE_DURATION_US 0
#define DIGIT_APPEARANCE_DURATION_US 300
#define COMMIT_IMPULSE_DURATION_US 2

static void turn_on_digit(uint8_t digit);

uint8_t display[] = {0xFF, 0xFF, 0xFF, 0xFF};
uint8_t current_symbol;

uint8_t digit_to_symbol(uint8_t digit) {
    switch (digit) {
        case 0:
            return _ZERO;
        case 1:
            return _ONE;
        case 2:
            return _TWO;
        case 3:
            return _THREE;
        case 4:
            return _FOUR;
        case 5:
            return _FIVE;
        case 6:
            return _SIX;
        case 7:
            return _SEVEN;
        case 8:
            return _EIGHT;
        case 9:
            return _NINE;
        default:
            return _OFF;
    }
}

static void set_display_output() {
    for (uint8_t i = 0; i < 8; i++) {
        if ((current_symbol >> i) & 1) {
            DS_DIG = 1;
        }else {
            DS_DIG = 0;
        }

        SH_CP_DIG = 1;
        __delay_us(SHIFT_IMPULSE_DURATION_US);
        SH_CP_DIG = 0;
    }
}

void init_display() {
    DIG1 = 0;
    DIG1_TRIS = 0;

    DIG2 = 0;
    DIG2_TRIS = 0;

    DIG3 = 0;
    DIG3_TRIS = 0;

    DIG4 = 0;
    DIG4_TRIS = 0;

    SH_CP_DIG = 0;
    SH_CP_DIG_TRIS = 0;

    DS_DIG = 0;
    DS_DIG_TRIS = 0;

    ST_CP_DIG = 0;
    ST_CP_DIG_TRIS = 0;

    current_symbol = 0xFF;

    set_display_output();
    commit_display();
}

static void turn_all_digits_off() {
    DIG1 = 0;
    DIG2 = 0;
    DIG3 = 0;
    DIG4 = 0;
}

void show_display() {
    for (uint8_t digit = 0; digit < 4; digit++) {
        current_symbol = display[digit];

        turn_all_digits_off();
       
        set_display_output();
        
        commit_display();

        turn_on_digit(digit);
        __delay_us(DIGIT_APPEARANCE_DURATION_US);
    }
    
    turn_all_digits_off();
}

void set_all_display_symbols(uint8_t symbol1, uint8_t symbol2, uint8_t symbol3, uint8_t symbol4) {
    display[0] = symbol1;
    display[1] = symbol2;
    display[2] = symbol3;
    display[3] = symbol4;
}

void set_display_symbol(uint8_t symbol, uint8_t digit) {
    display[digit] = symbol;
}

void commit_display() {
    ST_CP_DIG = 1;
    __delay_us(COMMIT_IMPULSE_DURATION_US);
    ST_CP_DIG = 0;
}

static void turn_on_digit(uint8_t digit) {
    switch (digit) {
        case 0:
            DIG1 = 1;
            DIG2 = 0;
            DIG3 = 0;
            DIG4 = 0;
            break;
        case 1:
            DIG1 = 0;
            DIG2 = 1;
            DIG3 = 0;
            DIG4 = 0;
            break;
        case 2:
            DIG1 = 0;
            DIG2 = 0;
            DIG3 = 1;
            DIG4 = 0;
            break;
        case 3:
            DIG1 = 0;
            DIG2 = 0;
            DIG3 = 0;
            DIG4 = 1;
            break;
    }
}